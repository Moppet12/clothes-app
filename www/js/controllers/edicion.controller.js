angular
.module('clothes')
.controller('edicionCtrl', edicionCtrl);

function edicionCtrl($scope, $state, $ionicHistory, $templateRequest, ngFB, 
	$cordovaToast,
	$cordovaScreenshot,
	$cordovaSocialSharing)
{
	var initialObject = [];
	$('.prenda img').each(function(i,e){
		initialObject[i] = {src: $(this).attr("src")};
	});
	$scope.storage = window.localStorage;

	$scope.nombreCategoria = $state.params.nombre;

	var templateUrl = 'templates/m-formal.html';

	var gender = $scope.storage.gender;

	(gender == 'f') ? gender = 'women' : gender = 'men';

	var obj = document.getElementsByClassName('clothes');
	var objC = document.getElementsByClassName('colors');
	var swiper = new Swiper(angular.element(obj),{
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		simulateTouch: false
	});

	var swiperC = new Swiper(angular.element(objC), {
		scrollbar: '.swiper-scrollbar',
		scrollbarHide: true,
		slidesPerView: 8,
		centeredSlides: true,
		spaceBetween: 10,
		grabCursor: true
	});

	// Persist

	var colorsCodes = ['bl','g','gr','ch','r','o','y','p','c','b','w','ox','k','go','d','br','pu','n'];

	var casual = {
		route : 'img/prendas/'+gender+'/casual/',
		name : 'casual',
		bl : '1,0,1,0,1,1,1,1,1,0',
		g : '1,1,0,0,1,1,1,1,0,0',
		gr : '1,0,1,0,0,0,1,0,0,0',
		ch : '0,0,0,1,0,1,1,1,0,0',
		r : '1,1,0,0,1,1,0,0,0,0',
		o : '1,1,0,1,1,1,0,0,0,0',
		y : '1,1,1,1,0,0,1,1,1,1',
		p : '1,1,0,1,0,0,1,1,1,1',
		c : '0,0,0,0,0,0,1,1,1,1',
		b : '0,0,0,0,0,0,1,1,1,1'
	}

	var sport = {
		route : 'img/prendas/'+gender+'/sport/',
		name : 'sport',
		bl : '1,0,1,0,1,1,1,1,1,0',
		g : '1,1,0,0,1,1,1,1,0,0',
		gr : '1,0,1,0,0,0,1,0,0,0',
		ch : '0,0,0,1,0,1,1,1,0,0',
		r : '1,1,0,0,1,1,0,0,0,0',
		o : '1,1,0,1,1,1,0,0,0,0,',
		y : '1,1,1,1,0,0,1,1,1,1',
		p : '1,1,0,1,0,0,1,1,1,1',
		c : '0,0,0,0,0,0,1,1,1,1',
		b : '0,0,0,0,0,0,1,1,1,1'
	}

	var formal = {
		route : 'img/prendas/'+gender+'/formal/',
		name : 'formal',
		m : {
			bl : {ca: 'all', za: 'n'},
			b : {ca: 'all', za: 'n'},
			c : {ca: 'all', za: 'n'},
			g : {ca: 'w', za: 'c', code: 'g'},
			ox : {ca: 'w', za: 'c', code: 'ox'}
		},
		w : {
			to : {

			},
			pi : {

			},
			ft : {

			}
		}

	}

	var coctel = {
		route : 'img/prendas/'+gender+'/coctel/',
		name : 'coctel',
	}

	var etiqueta = {
		route : 'img/prendas/'+gender+'/etiqueta/',
		name : 'etiqueta'
	}

	var globalClothe;
	var choosenClothe;
	var choosenClotheSrc;
	var initialWidth;
	var initialHeight;
	var category;
	var color;
	var fooIndex;

	$scope.chooseClothe = function(id){
		globalClothe = id;
		choosenClothe = $('#'+id);
		$('.active-clothe').css('display','none');
		choosenClothe.siblings('.active-clothe').css('display','inline');
		switch ($scope.nombreCategoria) {
			case 'Casual': choosenClotheSrc = casual.route+id+'/'; category = casual; break;
			case 'Deportiva': choosenClotheSrc = sport.route+id+'/'; category = sport; break;
			case 'Formal': if(id == 'sa_t') choosenClotheSrc = formal.route+'sa/'; else choosenClotheSrc = formal.route+id+'/'; category = formal;
			if(id == 'ca'){
				choosenClothe = $('.cl-ca');	
				$scope.toastShirt();
			}
			if(id == 'cr') choosenClothe = $('.cl-cr');
			break;
			case 'Coctel': choosenClotheSrc = coctel.route+id+'/';
			if (id == 'ca') choosenClothe = $('.cl-ca');
			if (id == 'sa') choosenClothe = $('.cl-sa');
			if (id == 'co') choosenClothe = $('.cl-co');
			category = coctel;
			break;
			case 'Etiqueta': choosenClotheSrc = etiqueta.route+id+'/'; category = etiqueta; break;
		}
		colorsCodes.forEach(function(e,i){
			fooIndex = i;
			$("#"+e+"Color").parent().removeClass('color-btn-disabled');
			$.get(choosenClotheSrc+e+'.png')
			.done(
				function(){
					if(fooIndex==colorsCodes.length-1){
						if(color){
							var temp = color.split(',');
							colorsCodes.forEach(function(el,ind){
								if(temp[ind]){
									if(temp[ind] == 0){
										$("#"+el+"Color").parent().addClass('color-btn-disabled');
									}
								}
							})
						}
					}
				}
				)
			.fail(function(){
				$("#"+e+"Color").parent().addClass('color-btn-disabled');
			});
		});
	}

	$scope.colorButton = function(code){
		var url = choosenClotheSrc+code+'.png';
		choosenClothe.attr('src',url);
		checkCategory(code);
		color = category[code];
	}

	function checkCategory(code){
		switch(category.name){
			case 'formal': formalRules(code); break;
			case 'coctel': coctelRules(code); break;
			case 'casual': casualRules(code); break;
		}
	}

	function formalRules(code){
		g = $scope.storage.gender;
		var selSa = ((($('.cl-sa').attr('src')).split('/'))[5]).split('.')[0];
		if(g == "m"){
			if(globalClothe == 'za'  && code)
				changeCiZa(code,formal);
			else if(globalClothe !== 'ca' && code && globalClothe !== 'cr'){
				$('.cl-sa').attr('src',formal.route+'sa/'+code+'.png');
				$('#pa').attr('src',formal.route+'pa/'+code+'.png');
				if(formal[g][code].code == 'g' || formal[g][code].code == "ox"){
					$('.cl-ca').attr('src',formal.route+'ca/w.png');
				}
			}
			else if(globalClothe == 'ca'){
				(selSa == 'g' || selSa == 'ox' ) ? $('.cl-ca').attr('src',formal.route+'ca/w.png') : null;
			}
		}
		else{
			// if(globalClothe == 'pa' || globalClothe == 'fa'){
			// 	switch(selSa){
			// 		case 'b': case 'c' : $('#blColor').parent().addClass('color-btn-disabled');
			// 				break;
			// 		case 'bl': case 'go': case 'g': $('#bColor').parent().addClass('color-btn-disabled');
			// 				$('#cColor').parent().addClass('color-btn-disabled');
			// 				break;
			// 	}
			// }
			if(globalClothe == 'ca'){
				switch(selSa){
					case 'bl': $('#bColor').parent().addClass('color-btn-disabled');
					$('#cColor').parent().addClass('color-btn-disabled');
					break;
					case 'c': case 'bl': $('#pColor').parent().addClass('color-btn-disabled');
					$('#blColor').parent().addClass('color-btn-disabled');
					default : break;
				}
			}

		}
	}

	function coctelRules(code){
		g = $scope.storage.gender;
		if(g == 'm'){
			var selSa = ($('.cl-sa').attr('src')).split('/')[5].split('.')[0];
			if(globalClothe == 'za' && code)
				changeCiZa(code,coctel);
			else if(globalClothe !== 'ca' && globalClothe !== 'co' && code){
				$('.cl-sa').attr('src',coctel.route+'sa/'+code+'.png');
				$('#pa').attr('src',coctel.route+'pa/'+code+'.png');
				if(coctel[g][code].code == 'g' || coctel[g][code].code == "ox"){
					$('.cl-ca').attr('src',coctel.route+'ca/w.png');
				}
			}
			else{
				switch(selSa){
					case 'g': $('#bColor').parent().addClass('color-btn-disabled');
					break;
					case 'br' : case 'k': $('#gColor').parent().addClass('color-btn-disabled');
				}
			}
		}
		else{
			var selVe = ($('#ve').attr('src')).split('/')[5].split('.')[0];
			if(globalClothe != 've'){
				switch(selVe){
					case 'r': case 'bl': case 'g': $('#brColor').parent().addClass('color-btn-disabled'); break;
					case 'd': case 'br': case 'bl': $('#brColor').parent().addClass('color-btn-disabled'); break;
				}
			}
		}
	}

	function casualRules(code){
		g = $scope.storage.gender;
		if(g == 'm'){
			if(globalClothe == 'za')
				changeCiZa(code,casual);
		}
	}

	function changeCiZa(code, category){
		var cin = $('#ci');
		var zap = $('#za');
		if(category.name == 'casual') cin = $('.ci');

		cin.attr('src',category.route+'ci/'+code+'.png');
		zap.attr('src',category.route+'za/'+code+'.png');
	}

	$scope.toastJeans = function(){
		try{
			$cordovaToast.showShortBottom('Esta prenda no cambia de color.');
		}
		catch(err){
			alert('Esta prenda no cambia de color.');
		}
	}

	$scope.toastShoes = function(){
		try{
			$cordovaToast.showShortBottom('El cinto y los zapatos comparten color.');
		}
		catch(err){
			alert('El cinto y los zapatos comparten color.');
		}
	}

	$scope.toastShirt = function(){
		try{
			$cordovaToast.showShortBottom('Esta prenda puede combinarse con saco.');
		}
		catch(err){
			alert('Esta prende puede combinarse con saco.');
		}
	}

	$scope.restartClothes = function(){
		$('.prenda img').each(function(i,e){
			$(this)[0].src = initialObject[i].src;
		});
	}

	$scope.fbShare = function(){
		$cordovaScreenshot.capture()
		.then(function(result) {

	          //on success you get the image url

	          //post on facebook (image & link can be null)
	          $cordovaSocialSharing
	          .share("", "DressCode", 'file://' + result, null)
	          .then(function(result) {
	          	alert.log("Se ha compartido con éxito");
	                        //do something on post success or ignore it...
	                    }, function(err) {
	                    	console.log(err);
	                    });
	      }, function(err) {
	      	alert("Ha ocurrido un error al tomar la captura");
	      });
	}

	$scope.edicionGoBack = function(){
		$state.go('categorias');
	}

	$scope.goRandom = function(category){
		$state.go('random', {category : category});
	}
}
