angular
	.module('clothes')
	.controller('categoriasCtrl', categoriasCtrl);

function categoriasCtrl($scope, $state, $stateParams, $ionicHistory, $ionicSideMenuDelegate){
	$scope.storage = window.localStorage;
	console.log('Categorias Loaded!');
	console.log($scope.storage);

	$scope.toggleLeft = function(){
		$ionicSideMenuDelegate.toggleLeft();
	}

	$scope.edicion = function(name, id, route){
		var thisGen = $scope.storage.getItem('gender');
		$ionicHistory.clearCache().then(function(){
			console.log(thisGen+'-'+route);
			$state.go(thisGen+'-'+route,{'nombre': name, 'id': id, 'route': thisGen+'-'+route});
		});	
	}

	$scope.logout = function(){
		window.localStorage.clear();
		$ionicHistory.clearCache();
		$ionicHistory.clearHistory();
		console.log($scope.storage);
		$state.go('login');
	}
}