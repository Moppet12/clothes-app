angular
	.module('clothes')
	.controller('randomCtrl', randomCtrl);

function randomCtrl($scope, $state, $ionicHistory, $templateRequest, ngFB){
	$scope.storage = window.localStorage;
	
	console.log('Param: '+$state.params.category);

	var gender = '';
	var route = '';
	var category = $state.params.category;
	var baseRoute;
	var categoryName;
	var random;
	var id;

	if($scope.storage.gender == 'm')
		gender = 'men';
	else
		gender = 'women';

	$scope.getRandomClothe = function(){
		console.log('called');
		random = Math.floor(Math.random() * 10) + 1;
		route = 'img/prendas/'+gender+'/'+category+'/rand/'+random+'.png';
		$.get(route).done(function(){
			$("#clothe").attr('src', route);
		}).fail(function(){
			getRandomClothe();
		});
	}


	switch(category){
		case 'casual': categoryName = 'Casual'; id = 2; baseRoute = category; break;
		case 'etiqueta' : categoryName = 'Etiqueta'; id = 3;baseRoute = category; break;
		case 'coctel': categoryName = 'Coctel'; id = 4;baseRoute = category; break;
		case 'formal' : categoryName = 'Formal'; id = 1;baseRoute = category; break;
		case 'sport' : categoryName = 'Deportiva'; id = 5;baseRoute = 'deportiva'; break;
	}

	$scope.goBack = function(){
		$state.go($scope.storage.gender+'-'+baseRoute,{nombre:categoryName, id:id, route:$scope.storage.gender+'-'+baseRoute});
	}

}