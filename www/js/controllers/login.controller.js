angular
	.module('clothes')
	.controller('loginCtrl',loginCtrl);

function loginCtrl($scope, $rootScope, $state, $ionicPopup, ngFB){
	window.localStorage.clear();

	var storage  = window.localStorage;

	//Si la sesión está iniciada: Redirigir
	
	$scope.chooseGender = function(g){
		$(".gender-btn").removeClass('gender-btn-enabled');
		if(g == 0){
			$("#female").addClass('gender-btn-enabled');
			$("#male").addClass('gender-btn-disabled');
			$scope.gender = 'f';
			$scope.femaleBtn = !$scope.femaleBtn;
		}
		else{
			$("#male").addClass('gender-btn-enabled');
			$("#female").addClass('gender-btn-disabled');
			$scope.gender = 'm';
			$scope.maleBtn = !$scope.maleBtn;
		}
		storage.setItem('gender', $scope.gender);
		console.log($scope.gender);
	}

	$scope.fbLogin = function(){
		// if($scope.gender == null){
		// 	alert('Selecciona Género');
		// }
		// else{
			ngFB.login({scope: 'public_profile,publish_actions'}).then(
				function(response){
					if(response.status === 'connected'){
						ngFB.api({
							path: '/me',
							params: {fields: 'id,name,gender'}
						})
						.then(
							function(user){
								var gender = '';
								$scope.user = user;
								console.log(user);
								storage.setItem('username',user.name);
								(user.gender == 'male') ? gender = 'm' : gender = 'f';
								storage.setItem('gender',gender);
								console.log(storage.getItem('gender'));
							},
							function(error){
								alert('Ha ocurrido un error.');
							}
						);
						$state.go('categorias');
					}
					else
						console.log('ERROR');
				});
		// }
	}
	$scope.omitir = function(){
		if(!storage.getItem('gender'))
			$ionicPopup.alert({
				title : 'Selecciona género',
				template : 'Por favor selecciona un género para continuar'
			});
		else{
			storage.setItem('username', 'Invitado');
			$state.go('categorias');
		}
	}

	$rootScope.logOut = function(){
		console.log('Will Logout');
	}
}