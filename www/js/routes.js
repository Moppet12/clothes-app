angular
	.module('clothes')
	.config(config);

function config($stateProvider, $urlRouterProvider){
	var globalParams = {
		'nombre' : 'Default',
		'id' : 0,
		'route' : ''
	};
	var ctrl = 'edicionCtrl';

	$stateProvider
		.state('login',{
		  url: '/login',
		  templateUrl: 'templates/login.html',
		  controller: 'loginCtrl' 
		})
		.state('categorias',{
			url: '/categorias',
			templateUrl: 'templates/categorias.html',
			controller: 'categoriasCtrl'
		})
		.state('m-formal',{
			url: '/m-formal',
			templateUrl: 'templates/m-formal.html',
			controller: ctrl,
			params: globalParams
		})
		.state('f-formal',{
			url: 'f-formal',
			templateUrl: 'templates/f-formal.html',
			controller: ctrl,
			params: globalParams
		})
		
		.state('m-casual',{
			url: '/m-casual',
			templateUrl: 'templates/m-casual.html',
			controller: ctrl,
			params: globalParams
		})
		.state('f-casual',{
			url: '/f-casual',
			templateUrl: 'templates/f-casual.html',
			controller: ctrl,
			params: globalParams
		})

		.state('m-etiqueta',{
			url: '/m-etiqueta',
			templateUrl: 'templates/m-etiqueta.html',
			controller: ctrl,
			params: globalParams
		})
		.state('f-etiqueta',{
			url: '/f-etiqueta',
			templateUrl: 'templates/f-etiqueta.html',
			controller: ctrl,
			params: globalParams
		})

		.state('m-coctel',{
			url: '/m-coctel',
			templateUrl: 'templates/m-coctel.html',
			controller: ctrl,
			params: globalParams
		})
		.state('f-coctel',{
			url: '/f-coctel',
			templateUrl: 'templates/f-coctel.html',
			controller: ctrl,
			params: globalParams
		})

		.state('m-deportiva',{
			url: '/m-deportiva',
			templateUrl: 'templates/m-sport.html',
			controller: ctrl,
			params: globalParams
		})
		.state('f-deportiva',{
			url: '/f-deportiva',
			templateUrl: 'templates/f-sport.html',
			controller: ctrl,
			params: globalParams
		})

		.state('random',{
			url: '/random',
			templateUrl: 'templates/random.html',
			controller: 'randomCtrl',
			params: {
				category: 'Default'
			}
		})

		.state('test',{
			url: '/test',
			templateUrl: 'templates/test.html',
			controller: ctrl
		});

    $urlRouterProvider.otherwise('/login');
}